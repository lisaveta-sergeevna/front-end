'use strict';

angular.
    module('studentTable').
    component('studentTable', {
        templateUrl: 'student-table/student-table.template.html',
        controller: ['$http', '$routeParams',
            function StudentTableController($http, $routeParams) {
                var self = this;

                self.showMenu = $routeParams.corpseAlias === undefined;

                self.orderProperty = null;
                self.reverseSort = true;
                self.showArrow = false;

                $http.get('https://lyceumexams.herokuapp.com/api/corpses/saved')
                    .then(function (response) {
                        self.corpses = response.data;
                    });

                $http.get('https://lyceumexams.herokuapp.com/api/corpses/saved/' + $routeParams.corpseAlias)
                    .then(function (response) {
                        self.corpse = response.data;
                        self.places = self.corpse.places;

                        self.audiences = [];

                        self.places.forEach(function (place) {
                            self.audiences = self.audiences.concat(place.audience);
                        });
                    });

                $http.get('https://lyceumexams.herokuapp.com/api/dictionary')
                    .then(function (response) {
                        self.dictionary = response.data;
                    });

                $http.get('https://lyceumexams.herokuapp.com/api/pupils/saved?corps=' + $routeParams.corpseAlias + '&place=')
                    .then(function (response) {
                        self.students = response.data;
                    });

                self.audienceBelongsPlace = function (audience) {
                    if (self.selectedPlace === undefined || self.selectedPlace === null) {
                        return true;
                    }

                    return self.selectedPlace.audience.some(function (item) {
                        return item._id === audience._id;
                    });
                };

                self.studentBelongsPlace = function (student) {
                    if (self.selectedPlace === undefined || self.selectedPlace === null) {
                        return true;
                    }

                    return student.place === self.selectedPlace._id;
                };

                self.studentBelongsAudience = function (student) {
                    if (self.selectedAudience === undefined || self.selectedAudience === null) {
                        return true;
                    }

                    return student.audience === self.selectedAudience._id;
                };

                self.sortBy = function (properties) {
                    self.expanded = false;
                    self.showArrow = true;

                    self.orderProperty = properties;
                    self.reverseSort = !self.reverseSort;
                };

                self.expandInfo = function (index) {
                    if (self.expandIndex === index) {
                        self.expanded = !self.expanded;
                    }
                    else {
                        self.expandIndex = index;
                        self.expanded = true;
                    }
                };

                self.isExpanded = function (index) {
                    return self.expanded && index === self.expandIndex;
                };
            }
        ]
    });