'use strict';

angular.
    module('lyceumApp').
    config(['$routeProvider',
        function config($routeProvider) {
            $routeProvider.
            when('/corpses', {
                template: '<student-table></student-table>'
            }).
            when('/corpses/:corpseAlias', {
                template: '<student-table></student-table>'
            }).
            otherwise('/corpses');
        }
    ]);