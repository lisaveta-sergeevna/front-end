import {Observer} from "./Observer.js";

export class SelectObserver extends Observer {
    constructor(selectId, keyAttribute, textAttribute) {
        super();

        this.select = document.getElementById(selectId);
        this.keyAttribute = keyAttribute;
        this.textAttribute = textAttribute;
    }

    getValue() {
        return this.select.value;
    }

    addListener(eventType, listener) {
        this.select.addEventListener(eventType, listener);
    }

    clearOptions() {
        this.select.innerHTML = "";
        this.createOption("all", "Все");
    }

    createOption(value, text) {
        let option = document.createElement("option");

        option.value = value;
        option.text = text;

        this.select.appendChild(option);
    }

    notify(data) {
        this.clearOptions();

        data.forEach(value => {
            this.createOption(value[this.keyAttribute], value[this.textAttribute]);
        });
    }
}