import {ObservableObject} from './ObservableObject.js';
import {SelectObserver} from "./SelectObserver.js";
import {TableObserver} from "./TableObserver.js";
import {TableBridge} from "./TableBridge.js";

const corpsesUrl = "http://lyceumexams.herokuapp.com/api/corpses";
const dictionaryUrl = "http://lyceumexams.herokuapp.com/api/dictionary";
const pupilsUrl = "http://lyceumexams.herokuapp.com/api/pupils";

const comparer = {
    string: function(a, b) {
        if (a < b) {
            return -1;
        }
        else if (a === b) {
            return 0;
        }
        else {
            return 1;
        }
    },
    number: function (a, b) {
        return a - b;
    }
}

function collectStudentsData(json, dictionary) {
    let students = [];

    json.forEach(student => {
        students.push({
            name: student["firstName"] + " " + student["lastName"] + " " + student["parentName"],
            room: dictionary["audiences"][student["audience"]],
            profile: dictionary["profiles"][student["profile"]],
            bel: student["needBel"]
        });
    });

    return students;
}

function loadStudents(students, dictionary, buildingId, profileId) {
    const studentsUrl = pupilsUrl
        + '?corps=' + (buildingId === 'all' ? '' : buildingId)
        + '&place=' + (profileId === 'all' ? '' : profileId);

    let studentsPromise = sendGetRequest(studentsUrl);

    studentsPromise.then(
        studentsJson => students.update(collectStudentsData(studentsJson, dictionary)),
        error => console.log(error.message)
    );
}

function collectRoomsData(profiles) {
    let rooms = [];

    profiles.forEach(value => {
        value["audience"].forEach(room => {
            rooms.push({
                name: room["name"],
                count: room["count"],
                max: room["max"],
                bel: room["bel"]
            });
        });
    });

    return rooms;
}

function loadCorpses(corpses, dictionary) {
    let buildingObserver = new SelectObserver("buildings", "alias","name");
    let profileObserver = new SelectObserver("profiles", "_id", "code");
    let roomsObserver = new SelectObserver("rooms", "_id", "name");

    let studentsObserver = new TableObserver("students", "linedTableRow", true);
    let roomsStatObserver = new TableObserver("roomsStatistics", "linedTableRow", false);

    let dndBridge = new TableBridge(studentsObserver, roomsStatObserver);

    dndBridge.bindTables(function (studentRow, roomRow) {
        console.log('Студент ' + studentRow.cells[1].textContent
            + ' перемещён в кабинет ' + roomRow.cells[0].textContent);
    });

    let buildings = new ObservableObject();
    buildings.subscribe(buildingObserver);

    let profile = new ObservableObject();``
    profile.subscribe(profileObserver);

    let room = new ObservableObject();
    room.subscribe(roomsObserver);
    room.subscribe(roomsStatObserver);

    let students = new ObservableObject();
    students.subscribe(studentsObserver);

    buildings.update(corpses);

    buildingObserver.addListener('change', function() {
        let buildingId = buildingObserver.getValue();

        let building = corpses.find(c => c["alias"] === buildingId);
        let profiles = [];

        if (buildingId !== "all") {
            profiles = building["places"];
        }

        profile.update(profiles);
        room.update(collectRoomsData(profiles));

        loadStudents(students, dictionary, buildingId, "all");
    });

    profileObserver.addListener('change', function() {
        let profileId = profileObserver.getValue();
        let buildingId = buildingObserver.getValue();

        let building = corpses.find(c => c["alias"] === buildingId);
        let profiles = building["places"];

        if (profileId !== "all") {
            profiles = profiles.filter(p => p["_id"] === profileId);
        }

        room.update(collectRoomsData(profiles));
        loadStudents(students, dictionary, buildingId, profileId);
    });

    studentsObserver.addHeaderListener('click', function () {
        let property = this.getAttribute("data-property");
        let comparerType = this.getAttribute("data-sort");
        let classes = this.classList;

        if (classes.contains("ascending") || classes.contains("descending")) {
            classes.toggle("ascending");
            classes.toggle("descending");

            students.update(students.getState().reverse());
        }
        else {
            studentsObserver.headers.forEach(header => {
                header.classList.remove("ascending");
                header.classList.remove("descending");
            });

            classes.add("ascending");

            if (comparer.hasOwnProperty(comparerType)) {
                students.update(students.getState().sort(function (a, b) {
                    a = a[property];
                    b = b[property];

                    return comparer[comparerType](a, b);
                }));
            }
        }
    });
}

function sendGetRequest(requestUrl) {
    return new Promise(function (resolve, reject) {
        const request = new XMLHttpRequest();

        request.open('GET', requestUrl);
        request.send();

        request.onload = function () {
            if (request.status === 200) {
                resolve(JSON.parse(request.responseText));
            }
        };

        request.onerror = () => reject(new Error(
            'Ошибка загрузки данных по адресу ' + requestUrl));
    });
}

window.addEventListener('DOMContentLoaded', function() {
    let corpsesPromise = sendGetRequest(corpsesUrl);
    let dictionaryPromise = sendGetRequest(dictionaryUrl);

    corpsesPromise.then(
        corpses => {
            dictionaryPromise.then(
                dictionary => loadCorpses(corpses, dictionary),
                error => console.log(error.message)
            );
        },
        error => console.log(error.message)
    );
});