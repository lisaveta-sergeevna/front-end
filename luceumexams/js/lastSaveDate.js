let date = new Date();

let options = {
    day: 'numeric',
    month: 'long',
    year: 'numeric'
};

document.getElementById("saveDate").textContent
    = "Дата последнего сохранения: " + date.toLocaleString("ru-RU", options);

