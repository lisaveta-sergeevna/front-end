export class ObservableObject {
    constructor () {
        this.state = [];
        this.subscribers = [];
    }

    getState() {
        return this.state;
    }

    update(state) {
        this.state = state;
        this.notifyAll(state);
    }

    subscribe (observer) {
        this.subscribers.push(observer);
    }

    unsubscribe (observer) {
        this.subscribers = this.subscribers.filter(subscriber => subscriber !== observer);
    }

    notifyAll (data) {
        this.subscribers.forEach(observer => observer.notify(data));
    }
}