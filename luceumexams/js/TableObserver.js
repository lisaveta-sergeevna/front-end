import {Observer} from "./Observer.js";

export class TableObserver extends Observer {
    constructor(tableId, rowClass, enumerateRows) {
        super();

        this.tableId = tableId;
        this.rowClass = rowClass;

        this.table = document.getElementById(tableId);
        this.tableBody = this.table.getElementsByTagName("tbody")[0];
        this.headers = Array.from(this.table.getElementsByTagName("th"));

        this.rowEventListeners = [];

        this.enumerateRows = enumerateRows;
        this.rowNum = 0;
    }

    getRowSelector() {
        return '#' + this.tableId + ' .' + this.rowClass;
    }

    clearTable() {
        this.rowNum = 0;
        this.tableBody.innerHTML = "";
    }

    addHeaderListener(eventType, listener) {
        this.headers.forEach(header => {
            header.addEventListener(eventType, listener);
        });
    }

    addRowListener(eventType, listener) {
        this.rowEventListeners.push({
            event: eventType,
            handler: listener
        });
    }

    addRowEventListeners(row) {
        this.rowEventListeners.forEach(listener => {
            row.addEventListener(listener.event, listener.handler);
        });
    }

    addCell(row, textContent) {
        let cell = document.createElement("td");
        let text = document.createTextNode(textContent);

        cell.appendChild(text);
        row.appendChild(cell);
    }

    addImageCell(row, imageSrc) {
        let cell = document.createElement("td");

        let image = new Image();
        image.src = imageSrc;
        image.alt = "бел";

        image.classList.add("img");
        image.classList.add("flag");

        cell.appendChild(image);
        row.appendChild(cell);
    }

    addRow(item) {
        let row = document.createElement("tr");
        row.classList.add(this.rowClass);

        this.addRowEventListeners(row);

        if (this.enumerateRows) {
            this.rowNum += 1;
            this.addCell(row, this.rowNum);
        }

        for (let key in item) {
            if (item.hasOwnProperty(key)) {
                let value = item[key];

                if (value instanceof Date) {
                    let options = {
                        weekday: 'short',
                        month: 'long',
                        year: 'numeric'
                    };

                    this.addCell(row, value.toLocaleString("ru-RU", options));
                }
                else if (typeof value === "boolean") {
                    if (value) {
                        this.addImageCell(row, "../resources/flag.png", "img flag");
                    }
                    else {
                        this.addCell(row, "");
                    }
                }
                else {
                    this.addCell(row, value);
                }
            }
        }

        this.tableBody.appendChild(row);
    }

    notify(data) {
        this.clearTable();

        data.forEach(item => {
            this.addRow(item);
        });
    }
}