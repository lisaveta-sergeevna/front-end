export class TableBridge {
    constructor(source, destination) {
        this.source = source;
        this.destination = destination;
    }

    bindTables(updateData) {
        let destSelector = this.destination.getRowSelector();

        document.addEventListener('selectstart', function (event) {
            event.preventDefault();
        });

        this.source.addRowListener('mousedown', function (event) {
            let rowClone = this.cloneNode(true);

            rowClone.style.width = 'inherited';
            rowClone.style.position = 'absolute';
            rowClone.style.zIndex = '1000';
            rowClone.style.opacity = '.7';

            document.body.append(rowClone);
            moveAt(event.pageX, event.pageY);

            function moveAt(pageX, pageY) {
                rowClone.style.left = pageX - rowClone.offsetWidth / 2 + 'px';
                rowClone.style.top = pageY - rowClone.offsetHeight / 2 + 'px';
            }

            let currentDroppable = null;

            function onMouseMove(event) {
                moveAt(event.pageX, event.pageY);

                rowClone.hidden = true;
                let elementBelow = document.elementFromPoint(event.clientX, event.clientY);
                rowClone.hidden = false;

                if (elementBelow === null) {
                    return;
                }

                let droppableBelow = elementBelow.closest(destSelector);

                if (currentDroppable !== droppableBelow) {
                    if (currentDroppable) {
                        currentDroppable.style.backgroundColor = 'inherit';
                    }

                    currentDroppable = droppableBelow;

                    if (currentDroppable) {
                        currentDroppable.style.backgroundColor = '#b3f0ff';
                    }
                }
            }

            document.addEventListener('mousemove', onMouseMove);

            rowClone.onmouseup = function () {
                document.removeEventListener('mousemove', onMouseMove);

                if (currentDroppable !== null) {
                    currentDroppable.style.backgroundColor = 'inherit';
                    updateData(this, currentDroppable);
                }

                document.body.removeChild(rowClone);
            };
        });
    }
}